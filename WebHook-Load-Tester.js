const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {
    const {body} = event;
    const { messageSubject, receivedAt, tenantId, messageAS2ID, stationAS2ID, partnerAS2ID } = JSON.parse(body);

    console.log(`Webhook endpoint called. body: ${messageAS2ID}`)
    try {
        let data = await ddb.put({
            TableName: "WebHook-Load-Test",
            Item: {
                station_as2_id: stationAS2ID,
                message_id: messageAS2ID,
                partner_as2_id: partnerAS2ID,
                received_at: receivedAt,
                tenant_id: tenantId,
                subject: messageSubject
            }
        }).promise();

        const response = {
            statusCode: 200,
            body: { "Success": true },
        }
        console.log(`Webhook endpoint successfully called. response: ${JSON.stringify(response)}`);
        return response;

    } catch (err) {
        console.error(`Error occurred. Error: ${err}`);
        const response = {
            statusCode: 500,
            body: err,
        }
        return response;
    }
    ;
};
